<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [
<!ENTITY dbstyle SYSTEM "/usr/lib/sgml/stylesheets/nwalsh-modular/print/docbook.dsl" CDATA DSSSL>
]>

<style-sheet>
<style-specification use="docbook">
<style-specification-body>

;;#######################################################################
;;#                                                                     #
;;#    Dave's Custom DocBook Stylesheet by Dave Mason dcm@redhat.com    #
;;#            Based on Norman Walsh's Modular Stylesheets              #
;;#                                                                     #
;;#######################################################################
;;
;;#######################################################################
;;#                                                                     #
;;#    You can format your document using this stylesheet by running    #
;;#                                                                     #
;;#            $ jade -t ps -d thisfile.dsl yoursgmlfile.sgml           #
;;#                                                                     #
;;#######################################################################


;;======================================
;;General Options
;;======================================

;;Do you want to print on both sides of the paper?
(define %two-side% 
 #t)

;;Do you want enumerated sections? (E.g, 1.1, 1.1.1, 1.2, etc.)
(define %section-autolabel% 
 #f)

;;What is the default extension for graphics?
(define %graphic-default-extension% 
  "eps")

;;Show URL links? If the text of the link and the URL are identical,
;;the parenthetical URL is suppressed.
(define %show-ulinks%
 #t)

;;Tex Backend on
(define tex-backend 
 #t)

;;Define Line Spacing
(define %line-spacing-factor% 1.1)

;;Define the Paragraph Style
(define para-style
  (style
   font-size: %bf-size%
   font-weight: 'medium
   font-posture: 'upright
   font-family-name: %body-font-family%
   line-spacing: (* %bf-size% %line-spacing-factor%)))

(define ($object-titles-after$)
  (list (normalize "figure")))

;;======================================
;;Book Options
;;======================================


;;Do you want a title page for a Book?
(define %generate-book-titlepage%
 #t)

;;Do you want a separate page for the title?
(define %generate-book-titlepage-on-separate-page%
 #t)

;;Generate Book TOC?
(define %generate-book-toc% 
 #t)

;;Do you want a TOC for the element part?
(define %generate-part-toc% 
 #f)

;;Do you want the part toc on the part titlepage or separate?
(define %generate-part-toc-on-titlepage%
 #t)

;;Generate Part Title Page?
(define %generate-part-titlepage% 
  #f)

;;Do you want the Part intro on the part title page?
(define %generate-partintro-on-titlepage%
  #t)

;;What elements should have a LOT?
(define ($generate-book-lot-list$)
  (list ;(normalize "table")
    ;(normalize "figure")
    ;(normalize "example")
    (normalize "equation")))

;;Do you want chapters enumerated?
(define %chapter-autolabel% 
 #t)


(define %chap-app-running-head-autolabel% 
  ;; REFENTRY chap-app-running-head-autolabel
  ;; PURP Put chapter labels in running heads?
  ;; DESC
  ;; If true, running heads on 'Chapter's and 
  ;; 'Appendix'es will include an automatic label.
  ;; 
  ;; In other words, if a 'Chapter' has no 'Label' attribute,
  ;; and '%chap-app-running-head-autolabel%'
  ;; is true, running heads will include the automatic label for the
  ;; 'Chapter'. If '%chap-app-running-head-autolabel%'
  ;; is false, only the 'Title' (or 'TitleAbbrev')
  ;; will appear in the running head.
  ;; /DESC
  ;; AUTHOR N/A
  ;; /REFENTRY
  #t)



;;======================================
;;Article Options
;;======================================

;;Do you want a title page for an Article?
(define %generate-article-titlepage%
 #t)

;;Do you want a separate page for the title?
(define %generate-article-titlepage-on-separate-page%
 #t)

;;Do you want the article toc on the titlepage or separate?
(define %generate-article-toc-on-titlepage%
 #t)

;;Do you want to start new page numbers with each article?
(define %article-page-number-restart%
 #f)

;;======================================
;;Columns
;;======================================

;;How many columns do you want?
(define %page-n-columns%
 1)

;;How much space between columns?
(define %page-column-sep%
 0.2in)

;;======================================
;;Fonts
;;======================================

;;Defines the general size of the text in the document. normal(10),
;;presbyopic(12), and large-type(24). 
(define %visual-acuity%
 "normal")

;;What font would you like for titles?
(define %title-font-family% 
  "Helvetica")

;;What font would you like for the body?
(define %body-font-family% 
 "Palatino")

;;What font would you like for mono-seq?
(define %mono-font-family% 
 "Courier New")

;;If the base fontsize is 10pt, and '%hsize-bump-factor%' is
;; 1.2, hsize 1 is 12pt, hsize 2 is 14.4pt, hsize 3 is 17.28pt, etc
(define %hsize-bump-factor% 
 1.2)
;;1.2

;;What size do you want the body fonts?
(define %bf-size%
 (case %visual-acuity%
    (("tiny") 8pt)
    (("normal") 10pt)
    (("presbyopic") 12pt)
    (("large-type") 24pt)))

(define-unit em %bf-size%)

;;======================================
;;Margins
;;======================================

(define %left-right-margin% 6pi)

;;How much indentation for the body?
(define %body-start-indent% 
 4pi)

;;How big is the left margin? (relative to physical page)
(define %left-margin% 
 8pi) ;white-paper-column
;; 8pi white paper-no-column
;;6pi

;;How big is the right margin? (relative to physical page)
(define %right-margin% 
 8pi) ;white-paper-column
;; 8pi white-paper-no-column
;;6pi

;;How big do you want the margin at the top?
(define %top-margin%
(if (equal? %visual-acuity% "large-type")
      7.5pi
      4pi))
;;6pi

;;How big do you want the margin at the bottom?
(define %bottom-margin% 
 (if (equal? %visual-acuity% "large-type")
      7.5pi 
      2pi))

;;9.5pi
;;8pi

;;Define the text width. (Change the elements in the formula rather
;;than the formula itself)
(define %text-width% 
 (- %page-width% (+ %left-margin% %right-margin%)))

;;Define the body width. (Change the elements in the formula rather
;;than the formula itself)
(define %body-width% 
 (- %text-width% %body-start-indent%))

;;Define distance between paragraphs
(define %para-sep% 
 (/ %bf-size% 2.0))

;;Define distance between block elements (figures, tables, etc.).
(define %block-sep% 
 (* %para-sep% 2.0))

;;Indent block elements?
(define %block-start-indent% 
  0pt)
;0pt

;;======================================
;;Admon Graphics
;;======================================

;;Do you want admon graohics on?
(define %admon-graphics%
 #f)

;;Where are the admon graphics?
(define %admon-graphics-path%
 "../images/")

;;Define admon graphics usage
(define ($admon-graphic$ #!optional (nd (current-node)))
  (cond ((equal? (gi nd) (normalize "tip"))
 (string-append %admon-graphics-path% "tip.gif"))
((equal? (gi nd) (normalize "note"))
 (string-append %admon-graphics-path% "note.gif"))
((equal? (gi nd) (normalize "important"))
 (string-append %admon-graphics-path% "important.gif"))
((equal? (gi nd) (normalize "caution"))
 (string-append %admon-graphics-path% "caution.gif"))
((equal? (gi nd) (normalize "warning"))
 (string-append %admon-graphics-path% "warning.gif"))
(else (error (string-append (gi nd) " is not an admonition.")))))

;;Width of admon graphics
;(define ($admon-graphic-width$ #!optional (nd (current-node)))
;  0.3in)

;;======================================
;;Quadding
;;======================================

;;What quadding do you want by default; start, center, justify, or end?
(define %default-quadding%
 'justify)

;;What quadding for component titles(Chapter, Appendix, etc)?
(define %component-title-quadding% 
 'start)

;;What quadding for section titles?
(define %section-title-quadding% 
 'start)

;;What quadding for section sub-titles?
(define %section-subtitle-quadding%
 'start)

;;What quadding for article title?
(define %article-title-quadding% 
 'center)

;;What quadding for division subtitles?
(define %division-subtitle-quadding% 
  'start)

;;What quadding for component subtitles?
(define %component-subtitle-quadding% 
  'center)

;;What quadding for article sub-titles?
(define %article-subtitle-quadding%
 'center)

;;======================================
;;Figures
;;======================================

(element FIGURE 
  (make display-group
	(with-mode figure-caption-mode
		   (process-first-descendant "TITLE"))
	(process-children)))

(element GRAPHIC ($image$))
(element INLINEGRAPHIC ($image$))

(define ($image$)
  (let ((fileref (attribute-string "fileref"))
	(entityref (attribute-string "entityref")))
    (make paragraph
	  space-before: %block-sep%
	  space-after: %block-sep%
	  (make external-graphic
		entity-system-id: (if fileref fileref
				    (if entityref 
					(entity-generated-system-id entityref)
				      ""))
		display?: #t
		display-alignment: 'start))))

(mode figure-caption-mode
  (element TITLE
    (let ((label (attribute-string "label" (ancestor "figure"))))
      (make paragraph
	    use: para-style
	    font-weight: 'bold
	    space-before: %block-sep%
	    space-after: %para-sep%
	    keep-with-next?: #t
	    (literal
	     (string-append
	      "Figure "
	      (if label
		  label
		(format-number (element-number (parent (current-node))) "1"))
	      ". "))
	    (process-children-trim)))))

(element (FIGURE TITLE) (empty-sosofo)) ; don't show caption below figure

;;======================================
;;Paper Options
;;======================================

;;What size paper do you need? A4, USletter, USlandscape, or RedHat?
(define %paper-type%
 "USletter")

;;Now define those paper types' width
(define %page-width%
 (case %paper-type%
    (("A4") 210mm)
    (("USletter") 8.5in)
    (("RedHat") 7.0in)
    (("USlandscape") 11in)))

;;Now define those paper types' height
(define %page-height%
 (case %paper-type%
    (("A4") 297mm)
    (("USletter") 11in)
    (("RedHat") 8.5in)
    (("USlandscape") 8.5in)))

;;======================================
;;Functions
;;======================================

(define (OLSTEP)
  (case
   (modulo (length (hierarchical-number-recursive "ORDEREDLIST")) 4)
	((1) 1.2em)
	((2) 1.2em)
	((3) 1.6em)
	((0) 1.4em)))

(define (ILSTEP) 1.0em)

(define (PROCSTEP ilvl)
  (if (> ilvl 1) 1.8em 1.4em))

(define (PROCWID ilvl)
  (if (> ilvl 1) 1.8em 1.4em))


(define ($comptitle$)
  (make paragraph
	font-family-name: %title-font-family%
	font-weight: 'bold
	font-size: (HSIZE 2)
	line-spacing: (* (HSIZE 2) %line-spacing-factor%)
	space-before: (* (HSIZE 2) %head-before-factor%)
	space-after: (* (HSIZE 2) %head-after-factor%)
	start-indent: 0pt
	first-line-start-indent: 0pt
	quadding: 'start
	keep-with-next?: #t
	(process-children-trim)))

;;Callouts are confusing in Postscript... fix them.
(define %callout-fancy-bug% 
 #f)

;;======================================
;;Non-printing Elements
;;======================================

(element COMMENT (empty-sosofo))
(element TITLEABBREV (empty-sosofo))
(element SUBTITLE (empty-sosofo))
(element SETINFO (empty-sosofo))
(element BOOKINFO (empty-sosofo))
(element BIBLIOENTRY (empty-sosofo))
(element BIBLIOMISC (empty-sosofo))
(element BOOKBIBLIO (empty-sosofo))
(element SERIESINFO (empty-sosofo))
(element DOCINFO (empty-sosofo))
(element ARTHEADER (empty-sosofo))
(element ADDRESS (empty-sosofo))


;;======================================
;;Formalpara titles
;;======================================


;;Change the way Formal Paragraph titles are displayed. The commented
;;out section will run the titles in the paragraphs. 
(element (formalpara title)
  ;(make sequence
  ;font-weight: 'bold
  ;($runinhead$))
  ($lowtitle$ 5))

;;======================================
;;Inlines
;;======================================

(element application ($mono-seq$))
(element command ($bold-seq$))
(element filename ($mono-seq$))
(element function ($mono-seq$))
(element guibutton ($bold-seq$))
(element guiicon ($bold-seq$))
(element guilabel ($italic-seq$))
(element guimenu ($bold-seq$))
(element guimenuitem ($bold-seq$))
(element hardware ($bold-mono-seq$))
(element keycap ($bold-seq$))
(element literal ($mono-seq$))
(element parameter ($italic-mono-seq$))
(element prompt ($mono-seq$))
(element symbol ($charseq$))
(element emphasis ($italic-seq$))

</style-specification-body>
</style-specification>
<external-specification id="docbook" document="dbstyle">
</style-sheet>