Understanding Memory usage in GNOME.
Miguel de Icaza (miguel@gnu.org)

People usually look at the memory sizes for the processes and
misinterpret the information.  This is a common mistake.

When talking about memory usage and the reports you get from the
operating system, you need to keep in mind the following terms and
what they mean:

	SIZE	This is the address space seen by the process.  If the
	        process maps 65 megs of memory, the address space of
	        the process will be reported as 65 megs of memory. 

		Even if only 100k of memory are actually used.  
	
	 	This bit of information is not usually very useful and
	        this is what most people believe is the actual memory
	        usage of a program: it is not.

	RSS	This is the "Resident Set Size".  This is the amount
	        of memory actually resident on memory.  This is a
	        better computation of what is actually loaded in
	        memory.

	  	This does not include any piece of code that have been
	        "swapped out".

		So, for example, if you have a program that uses 100k
	        of memory and the operating system discards or swaps
	        40k of memory, the RSS will be 60k.

		The RSS includes both the memory which is unique to
	        this process and the memory shared with other
	        processes.  In most modern Unix systems, a large part
	        of this accounts for the memory used by shared
	        libraries.  So it usually includes the amount of
	        memory that is used by other processes for the code
	        that is shared:  The libc is usually taken into
	        account.

		GNOME uses many shared libraries (this makes the
	        applications share more memory).

	SHARE   This is the amount of memory from the RSS that is
	        shared with other applications.

So, the actual memory used uniquely by an application is RSS - SHARE.
If two programs use library "A", then the process would also be
"responsible" for A/2 of the memory, but there are many variations
(which pieces of the library each program uses) that make this
computation hard.

A good approximation is thus "RSS - SHARE".

The file manager in GNOME for example uses the following libraries: 

CORBA libraries:
        libgnorba.so.27 => /opt/gnome/lib/libgnorba.so.27 (0x40007000)
        libORBitCosNaming.so.0 =>/opt/gnome/lib/libORBitCosNaming.so.0 (0x40011000)
        libORBit.so.0 => /opt/gnome/lib/libORBit.so.0 (0x4001a000)
        libIIOP.so.0 => /opt/gnome/lib/libIIOP.so.0 (0x4007c000)
        libORBitutil.so.0 => /opt/gnome/lib/libORBitutil.so.0(0x40086000)

GNOME graphical libraries:
        libgnomeui.so.32 => /opt/gnome/lib/libgnomeui.so.32(0x40089000)
        libart_lgpl.so.2 => /opt/gnome/lib/libart_lgpl.so.2(0x4013b000)
        libgdk_imlib.so.1 => /opt/gnome/lib/libgdk_imlib.so.1(0x40149000)

Session management libraries:
        libSM.so.6 => /usr/X11R6/lib/libSM.so.6 (0x4016a000)
        libICE.so.6 => /usr/X11R6/lib/libICE.so.6 (0x40173000)

GTK libraries:
        libgtk-1.2.so.0 => /usr/local/lib/libgtk-1.2.so.0 (0x40188000)
        libgdk-1.2.so.0 => /usr/local/lib/libgdk-1.2.so.0 (0x402a3000)

Glib libraries:
        libgmodule-1.2.so.0 => /usr/local/lib/libgmodule-1.2.so.0(0x402d5000)
        libglib-1.2.so.0 => /usr/local/lib/libglib-1.2.so.0(0x403c9000)

X11 libraries:
        libXext.so.6 => /usr/X11R6/lib/libXext.so.6 (0x402d9000)
        libX11.so.6 => /usr/X11R6/lib/libX11.so.6 (0x402e7000)

GNOME libraries:
        libz.so.1 => /usr/lib/libz.so.1 (0x404a4000)
        libgnome.so.32 => /opt/gnome/lib/libgnome.so.32 (0x403a3000)
        libgnomesupport.so.0 => /opt/gnome/lib/libgnomesupport.so.0(0x403b5000)
        libdb.so.2 => /lib/libdb.so.2 (0x403bb000)

Standard Unix libraries:
        libdl.so.2 => /lib/libdl.so.2 (0x403ea000)
        libm.so.6 => /lib/libm.so.6 (0x4038a000)
        libc.so.6 => /lib/libc.so.6 (0x403ff000)
        /lib/ld-linux.so.2 => /lib/ld-linux.so.2 (0x2aaaa000)

File manager libraries:
        libext2fs.so.2 => /lib/libext2fs.so.2 (0x403ed000)
        libcom_err.so.2 => /lib/libcom_err.so.2 (0x403fd000)

So, even if it does not use all of the code, the "SIZE" will reflect
the maximal size the program would be using if it used all of those
libraries.  

The more libraries we use the bigger the SIZE goes, even if this does
not reflect the actual memory usage.
